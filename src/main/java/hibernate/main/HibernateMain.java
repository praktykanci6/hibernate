package hibernate.main;

import database.models.Addresses;
import database.models.People;
import database.models.Students;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import hibernate.util.HibernateUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Konrad on 02-04-2016.
 */
public class HibernateMain {

    public static void main(String[] args){
        /*Date date = new Date();
        Addresses adr1 = new Addresses();
        adr1.setCountry("Polska");
        adr1.setBuildingNumber("11");
        adr1.setTown("Tarnów");
        adr1.setStreet("Wałowa");
        adr1.setPostCode("33-100");
        adr1.setPhoneNumber1("500508002");

        People people = new People();
        people.setBirthDate(date);
        people.setFirstName("Jakub");
        people.setSurName("Kowalski");
        people.setEmail("costam@gmail.com");

        Students students = new Students();
        students.setAddresses(adr1);
        students.setPeople(people);*/
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        session.beginTransaction();

        session.getTransaction().commit();
        sessionFactory.close();
    }

}
