package hibernate.util;

import database.models.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by Konrad on 02-04-2016.
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory(){
        try{
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            configuration.addAnnotatedClass(database.models.Addresses.class);
            configuration.addAnnotatedClass(database.models.Students.class);
            configuration.addAnnotatedClass(database.models.People.class);
            configuration.addAnnotatedClass(database.models.AcademicYears.class);
            configuration.addAnnotatedClass(database.models.CoursesOfStudy.class);
            configuration.addAnnotatedClass(database.models.Practices.class);
            configuration.addAnnotatedClass(database.models.ProfessionalTitles.class);
            configuration.addAnnotatedClass(database.models.StudyMode.class);
            configuration.addAnnotatedClass(database.models.SubjectStudents.class);
            configuration.addAnnotatedClass(database.models.YearbooksStudies.class);
            configuration.addAnnotatedClass(database.models.LevelsOfStudies.class);
            configuration.addAnnotatedClass(database.models.PracticeCoordinators.class);
            configuration.addAnnotatedClass(database.models.Users.class);
            configuration.addAnnotatedClass(database.models.Roles.class);
            configuration.addAnnotatedClass(database.models.DirectedCoordinators.class);
            configuration.addAnnotatedClass(database.models.DirectedCoordinatorsOfPracticeDonors.class);
            configuration.addAnnotatedClass(database.models.PracticeDonors.class);
            configuration.addAnnotatedClass(database.models.PracticeDonorsProfiles.class);
            configuration.addAnnotatedClass(database.models.Profile.class);
            configuration.addAnnotatedClass(database.models.PracticeDonorsAddresses.class);
            configuration.addAnnotatedClass(database.models.PracticeKeepers.class);
            configuration.addAnnotatedClass(database.models.DirectedKeepers.class);
            configuration.addAnnotatedClass(database.models.TypesOfRequest.class);
            configuration.addAnnotatedClass(database.models.Agreements.class);
            configuration.addAnnotatedClass(database.models.Statuses.class);
            configuration.addAnnotatedClass(database.models.Qualifications.class);
            configuration.addAnnotatedClass(database.models.PracticeTemplates.class);
            configuration.addAnnotatedClass(database.models.CalendarUnits.class);
            configuration.addAnnotatedClass(database.models.DirectionalPractices.class);
            configuration.addAnnotatedClass(database.models.TypesOfPractices.class);

            System.out.println("Hibernate Configuration loaded");

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            System.out.println("Hibernate serviceRegistry Created");

            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            return sessionFactory;
        }
        catch (Throwable ex){
            System.err.println("Initial SessionFactory failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory(){
        if(sessionFactory == null){
            sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }
}
