package database.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Konrad on 23-04-2016.
 */
@Entity
@Table(name = "opiekunowie_kierunkowi",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_kierunku","id_opiekuna_praktyk","id_praktykodawcy"})})
public class DirectedKeepers implements Serializable{

    @EmbeddedId
    private DirectedKeepersId directedKeepersId;

    @ManyToOne
    @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
    private CoursesOfStudy directoryId;

    @ManyToOne
    @JoinColumn(name = "id_opiekuna_praktyk", referencedColumnName = "id_opiekuna_praktyk", insertable = false, updatable = false)
    private PracticeKeepers practiceKeeperId;

    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    private PracticeDonors practiceDonorId;

    public DirectedKeepersId getDirectedKeepersId() {
        return directedKeepersId;
    }

    public void setDirectedKeepersId(DirectedKeepersId directedKeepersId) {
        this.directedKeepersId = directedKeepersId;
    }

    public CoursesOfStudy getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(CoursesOfStudy directoryId) {
        this.directoryId = directoryId;
    }

    public PracticeKeepers getPracticeKeeperId() {
        return practiceKeeperId;
    }

    public void setPracticeKeeperId(PracticeKeepers practiceKeeperId) {
        this.practiceKeeperId = practiceKeeperId;
    }

    public PracticeDonors getPracticeDonorId() {
        return practiceDonorId;
    }

    public void setPracticeDonorId(PracticeDonors practiceDonorId) {
        this.practiceDonorId = practiceDonorId;
    }
}
