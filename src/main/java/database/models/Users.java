package database.models;

import javax.persistence.*;

/**
 * Created by Misiek on 16.04.2016.
 */
@Entity
@Table(name = "uzytkownicy", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_uzytkownika"})})
public class Users
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "members_sequence")
    @SequenceGenerator(name = "members_sequence",sequenceName = "members_sequence", allocationSize = 1)
    @Column(name = "id_uzytkownika", nullable = false, unique = true)
    private Integer idMember;

    @OneToOne
    @JoinColumn(name = "id_osoby")
    private People person;

    @ManyToOne
    @JoinColumn(name = "id_roli", referencedColumnName = "id_roli", insertable = false, updatable = false)
    private Roles roleId;

    @Column(name = "login", nullable = false,length = 20)
    private String login;

    @Column(name = "haslo", nullable = false,length = 80)
    private String password;

    @Column(name = "salt", nullable = false,length = 40)
    private String salt;

    @Column(name = "blokada")
    private Boolean blockade;

    public Integer getIdMember() {return idMember;}

    public void setIdMember(Integer idMember) {this.idMember = idMember;}

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public Roles getRoleId() {
        return roleId;
    }

    public void setRoleId(Roles roleId) {
        this.roleId = roleId;
    }

    public String getLogin() {return login;}

    public void setLogin(String login) {this.login = login;}

    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public String getSalt() {return salt;}

    public void setSalt(String salt) {this.salt = salt;}

    public Boolean getBlockade() {return blockade;}

    public void setBlockade(Boolean blockade) {this.blockade = blockade;}


}
