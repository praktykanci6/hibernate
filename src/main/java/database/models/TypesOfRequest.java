package database.models;

import javax.persistence.*;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "typy_zgloszen", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_typu_zgloszenia"})})
public class TypesOfRequest
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "typesOfRequest_sequence")
    @SequenceGenerator(name = "typesOfRequest_sequence",sequenceName = "typesOfRequest_sequence", allocationSize = 1)
    @Column(name = "id_typu_zgloszenia", nullable = false, unique = true)
    private Integer idTypeOfRequest;

    @Column(name = "typy_zgloszenia", nullable = false, unique = true)
    private String typeOfRequest;

    public Integer getIdTypeOfRequest() {return idTypeOfRequest;}

    public void setIdTypeOfRequest(Integer idTypeOfRequest) {this.idTypeOfRequest = idTypeOfRequest;}

    public String getTypeOfRequest() {return typeOfRequest;}

    public void setTypeOfRequest(String typeOfRequest) {this.typeOfRequest = typeOfRequest;}
}
