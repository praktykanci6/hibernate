package database.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Konrad on 11-04-2016.
 */

@Entity
@Table(name = "osoby", uniqueConstraints = {@UniqueConstraint(columnNames = ("id_osoby"))})
public class People {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "people_sequence")
    @SequenceGenerator(name = "people_sequence", sequenceName = "people_sequence", allocationSize = 1)

    @Column(name = "id_osoby", nullable = false, unique = true)
    private Integer personId;

    @Column(name = "data_urodzenia")
    private Date birthDate;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "imie", nullable = false)
    private String firstName;

    @Column(name = "nazwisko", nullable = false)
    private String surName;

    @Column(name = "telefon_komorkowy")
    private String cellPhoneNumber;

    @Column(name = "telefon_stacjonarny")
    private String phoneNumber;

    @Column(name = "tyul_zawodoy")
    private String jobTitle;

    @OneToOne(mappedBy = "people")
    private Students students;

    @OneToOne(mappedBy = "person")
    private Users user;

    @OneToOne(mappedBy = "person")
    private PracticeCoordinators practiceCoordinator;

    @OneToOne(mappedBy = "person")
    private PracticeKeepers practiceKeeper;



    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }

    public PracticeCoordinators getPracticeCoordinator() {
        return practiceCoordinator;
    }

    public void setPracticeCoordinator(PracticeCoordinators practiceCoordinator) {
        this.practiceCoordinator = practiceCoordinator;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public PracticeKeepers getPracticeKeeper() {
        return practiceKeeper;
    }

    public void setPracticeKeeper(PracticeKeepers practiceKeeper) {
        this.practiceKeeper = practiceKeeper;
    }
}
