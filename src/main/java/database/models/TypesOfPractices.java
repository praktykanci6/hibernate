package database.models;

import javax.persistence.*;

/**
 * Created by Misiek on 16.04.2016.
 */

import java.util.Set;

/**
 * Created by Konrad on 17-04-2016.
 */
@Entity
@Table(name = "typy_praktyk", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_typu_praktyki"})})
public class TypesOfPractices
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qualifications_sequence")
    @SequenceGenerator(name = "qualifications_sequence",sequenceName = "qualifications_sequence", allocationSize = 1)

    @Column(name = "id_typu_praktyki", nullable = false, unique = true)
    private Integer idTypesOfPractices;

    @Column(name = "typy_praktyki", nullable = false,length = 20)
    private String typesOfPractices;

    @OneToMany(mappedBy = "typeOfPracticeId")
    private Set<DirectionalPractices> directionalPractices;

    public Integer getIdTypesOfPractices() {return idTypesOfPractices;}

    public void setIdTypesOfPractices(Integer idTypesOfPractices) {this.idTypesOfPractices = idTypesOfPractices;}

    public String getTypesOfPractices() {return typesOfPractices;}

    public void setTypesOfPractices(String typesOfPractices) {this.typesOfPractices = typesOfPractices;}

    public Set<DirectionalPractices> getDirectionalPractices() {
        return directionalPractices;
    }

    public void setDirectionalPractices(Set<DirectionalPractices> directionalPractices) {
        this.directionalPractices = directionalPractices;
    }
}
