package database.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Konrad on 14-04-2016.
 */
@Embeddable
public class SubjectStudentsId implements Serializable {
    @Column(name = "nr_albumu")
    private Long albumNumberId;

    @Column(name = "id_kierunku")
    private Integer subjectId;

    @Column(name = "id_roku_akademickiego")
    private Integer academicYearId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectStudentsId that = (SubjectStudentsId) o;

        if (!albumNumberId.equals(that.albumNumberId)) return false;
        if (!subjectId.equals(that.subjectId)) return false;
        return academicYearId.equals(that.academicYearId);

    }

    @Override
    public int hashCode() {
        int result = albumNumberId.hashCode();
        result = 31 * result + subjectId.hashCode();
        result = 31 * result + academicYearId.hashCode();
        return result;
    }
}
