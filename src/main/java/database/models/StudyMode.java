package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Misiek on 13.04.2016.
 */

@Entity
@Table(name = "tryby_studiow", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_trybu_studiow"})})
public class StudyMode
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "studyMode_sequence")
    @SequenceGenerator(name = "studyMode_sequence",sequenceName = "studyMode_sequence", allocationSize = 1)

    @Column(name = "id_trybu_studiow", nullable = false, unique = true)
    private Integer idStudyMode;

    @Column(name = "tryb_studiow", nullable = false, length = 30)
    private String studyMode;

    @OneToMany(mappedBy = "studyMode")
    private Set<CoursesOfStudy> courseOfStudies;



    public Integer getIdStudyMode() {return idStudyMode;}

    public void setIdStudyMode(Integer idStudyMode) {this.idStudyMode = idStudyMode;}

    public String getStudyMode() {return studyMode;}

    public void setStudyMode(String studyMode) {this.studyMode = studyMode;}

    public Set<CoursesOfStudy> getCourseOfStudies() {
        return courseOfStudies;
    }

    public void setCourseOfStudies(Set<CoursesOfStudy> courseOfStudies) {
        this.courseOfStudies = courseOfStudies;
    }
}
