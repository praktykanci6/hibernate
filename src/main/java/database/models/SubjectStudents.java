package database.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Misiek on 11.04.2016.
 */

@Entity
@Table(name = "studenci_kierunkow",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_kierunku","nr_albumu","id_roku_akademickiego"})})
public class SubjectStudents implements Serializable
{
    @EmbeddedId
    private SubjectStudentsId subjectStudentsId;

    @ManyToOne
    @JoinColumn(name = "nr_albumu", referencedColumnName = "nr_albumu", insertable = false, updatable = false)
    private Students students;


    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false),
            @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
            })
    private YearbooksStudies yearBooksStudies;


    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }

    public SubjectStudentsId getSubjectStudentsId() {
        return subjectStudentsId;
    }

    public void setSubjectStudentsId(SubjectStudentsId subjectStudentsId) {
        this.subjectStudentsId = subjectStudentsId;
    }

    public YearbooksStudies getYearBooksStudies() {
        return yearBooksStudies;
    }

    public void setYearBooksStudies(YearbooksStudies yearBooksStudies) {
        this.yearBooksStudies = yearBooksStudies;
    }
}
