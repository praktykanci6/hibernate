package database.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by Konrad on 18-04-2016.
 */
@Embeddable
public class DirectedCoordinatorsId implements Serializable {

    @Column(name = "id_kierunku")
    private Integer directionId;

    @Column(name = "id_koordynatora_praktyk")
    private BigInteger practiceCoordinatorId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DirectedCoordinatorsId that = (DirectedCoordinatorsId) o;

        if (!directionId.equals(that.directionId)) return false;
        return practiceCoordinatorId.equals(that.practiceCoordinatorId);

    }

    @Override
    public int hashCode() {
        int result = directionId.hashCode();
        result = 31 * result + practiceCoordinatorId.hashCode();
        return result;
    }
}
