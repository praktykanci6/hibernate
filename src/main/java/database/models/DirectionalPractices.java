package database.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Konrad on 17-04-2016.
 */
@Entity
@Table(name = "praktyki_kierunkowe", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_kierunku", "id_typu_praktyk", "id_roku_akademickiego"})})
public class DirectionalPractices {

    @EmbeddedId
    private DirectionalPracticesId directionalPracticesId;

    @ManyToOne
    @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
    private YearbooksStudies directionId;

    @ManyToOne
    @JoinColumn(name = "id_typu_praktyki", referencedColumnName = "id_typu_praktyki", insertable = false, updatable = false)
    private TypesOfPractices typeOfPracticeId;

    @ManyToOne
    @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)
    private AcademicYears academicYearId;

    @Column(name = "min_data_rozpoczecia")
    private Date minimumStartDate;

    @Column(name = "max_data_zakonczenia")
    private Date maximumEndDate;

    @Column(name = "liczba_godzin")
    private Integer amountOFHours;

    @Column(name = "czas_trwania")
    private Integer duration;

    @ManyToOne
    @JoinColumn(name = "id_jednostki")
    private CalendarUnits unitId;
}
