package database.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Konrad on 20-04-2016.
 */
@Embeddable
public class DirectedCoordinatorsOfPracticeDonorsId implements Serializable{

    @Column(name = "id_praktykodawcy")
    private Integer practiceDonorId;

    @Column(name = "id_kierunku")
    private Integer directionId;

    @Column(name = "id_koordynatora_praktyk")
    private Integer practiceDonorCoordinatorId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DirectedCoordinatorsOfPracticeDonorsId that = (DirectedCoordinatorsOfPracticeDonorsId) o;

        if (!practiceDonorId.equals(that.practiceDonorId)) return false;
        if (!directionId.equals(that.directionId)) return false;
        return practiceDonorCoordinatorId.equals(that.practiceDonorCoordinatorId);

    }

    @Override
    public int hashCode() {
        int result = practiceDonorId.hashCode();
        result = 31 * result + directionId.hashCode();
        result = 31 * result + practiceDonorCoordinatorId.hashCode();
        return result;
    }
}
