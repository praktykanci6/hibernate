package database.models;

import javax.persistence.*;

/**
 * Created by Konrad on 20-04-2016.
 */
@Entity
@Table(name = "profile_praktykodawcow",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_praktykodawcy","id_profilu"})})
public class PracticeDonorsProfiles {

    @EmbeddedId
    private PracticeDonorsProfilesId practiceDonorsProfilesId;

    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    private PracticeDonors practiceDonorId;

    @ManyToOne
    @JoinColumn(name = "id_profilu", referencedColumnName = "id_profilu", insertable = false, updatable = false)
    private Profile profileId;

    public PracticeDonors getPracticeDonorId() {
        return practiceDonorId;
    }

    public void setPracticeDonorId(PracticeDonors practiceDonorId) {
        this.practiceDonorId = practiceDonorId;
    }

    public Profile getProfileId() {
        return profileId;
    }

    public void setProfileId(Profile profileId) {
        this.profileId = profileId;
    }
}
