package database.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Konrad on 20-04-2016.
 */
@Embeddable
public class PracticeDonorsAddressesId implements Serializable{

    @Column(name = "id_adresu")
    private Integer addressId;

    @Column(name = "id_praktykodawcy")
    private Integer practiceDonorId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PracticeDonorsAddressesId that = (PracticeDonorsAddressesId) o;

        if (!addressId.equals(that.addressId)) return false;
        return practiceDonorId.equals(that.practiceDonorId);

    }

    @Override
    public int hashCode() {
        int result = addressId.hashCode();
        result = 31 * result + practiceDonorId.hashCode();
        return result;
    }
}
