package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Misiek on 13.04.2016.
 */
@Entity
@Table(name = "tytuly_zawodowe", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_tytulu_zawodowego"})})
public class ProfessionalTitles
{
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "professionalTitles_sequence")
    @SequenceGenerator(name = "professionalTitles_sequence",sequenceName = "professionalTitles_sequence", allocationSize = 1)

    @Column(name = "id_tytulu_zawodowego", nullable = false, unique = true)
    private Integer idProfessionalTitle;

    @Column(name = "tytul_zawodowy", nullable = false, length = 30)
    private String professionalTitle;

    @OneToMany(mappedBy = "professionalTitle")
    private Set<CoursesOfStudy> courseOfStudies;


    public Integer getIdProfessionalTitle() {return idProfessionalTitle;}

    public void setIdProfessionalTitle(Integer idProfessionalTitle) {this.idProfessionalTitle = idProfessionalTitle;}

    public String getProfessionalTitle() {return professionalTitle;}

    public void setProfessionalTitle(String professionalTitle) {this.professionalTitle = professionalTitle;}


}
