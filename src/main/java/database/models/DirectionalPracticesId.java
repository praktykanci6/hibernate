package database.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Konrad on 17-04-2016.
 */
@Embeddable
public class DirectionalPracticesId implements Serializable{

    @Column(name = "id_kierunku")
    private Integer directionId;

    @Column(name = "id_typu_praktyki")
    private Integer typeOfPracticeId;

    @Column(name = "id_roku_akademickiego")
    private Integer academicYearId;
}
