package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Konrad on 17-04-2016.
 */
@Entity
@Table(name = "jednostki_kalendarzowe", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_jednostki_kalendarzowej"})})
public class CalendarUnits {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "calendarUnits_sequence")
    @SequenceGenerator(name = "calendarUnits_sequence", sequenceName = "calendarUnits_sequence", allocationSize = 1)

    @Column(name = "id_jednostki_kalendarzowej", nullable = false, unique = true)
    private Integer calendarUnitId;

    @Column(name = "jednostka_kalendarzowa", nullable = false, length = 15)
    private String calendarUnit;

    @OneToMany(mappedBy = "unitId")
    private Set<DirectionalPractices> directionalPractices;

    public Integer getCalendarUnitId() {
        return calendarUnitId;
    }

    public void setCalendarUnitId(Integer calendarUnitId) {
        this.calendarUnitId = calendarUnitId;
    }

    public String getCalendarUnit() {
        return calendarUnit;
    }

    public void setCalendarUnit(String calendarUnit) {
        this.calendarUnit = calendarUnit;
    }

    public Set<DirectionalPractices> getDirectionalPractices() {
        return directionalPractices;
    }

    public void setDirectionalPractices(Set<DirectionalPractices> directionalPractices) {
        this.directionalPractices = directionalPractices;
    }
}
