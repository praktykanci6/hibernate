package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Misiek on 13.04.2016.
 */

@Entity
@Table(name = "roczniki_studiow",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_kierunku","id_roku_akademickiego"})})
public class YearbooksStudies
{
    @EmbeddedId
    private YearbooksStudiesId yearbooksStudiesId;

    @ManyToOne
    @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)
    private AcademicYears academicYears;

    @ManyToOne
    @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
    private CoursesOfStudy coursesOfStudy;

    @OneToMany(mappedBy = "yearBooksStudies")
    private Set<SubjectStudents> subjectStudents;



    public YearbooksStudiesId getYearbooksStudiesId() {
        return yearbooksStudiesId;
    }

    public void setYearbooksStudiesId(YearbooksStudiesId yearbooksStudiesId) {
        this.yearbooksStudiesId = yearbooksStudiesId;
    }

    public AcademicYears getAcademicYears() {
        return academicYears;
    }

    public void setAcademicYears(AcademicYears academicYears) {
        this.academicYears = academicYears;
    }

    public CoursesOfStudy getCoursesOfStudy() {
        return coursesOfStudy;
    }

    public void setCoursesOfStudy(CoursesOfStudy coursesOfStudy) {
        this.coursesOfStudy = coursesOfStudy;
    }

    public Set<SubjectStudents> getSubjectStudents() {
        return subjectStudents;
    }

    public void setSubjectStudents(Set<SubjectStudents> subjectStudents) {
        this.subjectStudents = subjectStudents;
    }
}
