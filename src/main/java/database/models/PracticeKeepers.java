package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Konrad on 23-04-2016.
 */
@Entity
@Table(name = "opiekunowie_praktyk", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_opiekuna_praktyk"})})
public class PracticeKeepers {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "practiceKeppers_sequence")
    @SequenceGenerator(name = "practiceKeppers_sequence",sequenceName = "practiceKeppers_sequence", allocationSize = 1)
    @Column(name = "id_opiekuna_praktyk", unique = true)
    private Integer practiceKeeperId;

    @Column(name = "stanowisko", unique = true,length = 50)
    private String position;

    @OneToOne
    @JoinColumn(name = "id_osoby")
    private People person;

    @OneToMany(mappedBy = "practiceKeeperId")
    private Set<DirectedKeepers> directedKeepers;

    public Integer getPracticeKeeperId() {
        return practiceKeeperId;
    }

    public void setPracticeKeeperId(Integer practiceKeeperId) {
        this.practiceKeeperId = practiceKeeperId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public Set<DirectedKeepers> getDirectedKeepers() {
        return directedKeepers;
    }

    public void setDirectedKeepers(Set<DirectedKeepers> directedKeepers) {
        this.directedKeepers = directedKeepers;
    }
}
