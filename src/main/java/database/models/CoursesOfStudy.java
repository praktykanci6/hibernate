package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Misiek on 13.04.2016.
 */

@Entity
@Table(name = "kierunki_studiow",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_kierunku"})})
public class CoursesOfStudy
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coursesOfStudy_sequence")
    @SequenceGenerator(name = "coursesOfStudy_sequence",sequenceName = "coursesOfStudy_sequence", allocationSize = 1)
    @Column(name = "id_kierunku", nullable = false, unique = true)
    private Integer idDirection;

    @Column(name = "nazwa_kierunku", nullable = false, length = 50)
    private String nameDirection;

    @Column(name = "nazwa_specjalnosci", nullable = false, length = 50)
    private String nameSpecialty;

    @ManyToOne
    @JoinColumn(name = "id_stopnia_studiow", nullable = false, unique = true)
    private LevelsOfStudies levelOfStudies;

    @ManyToOne
    @JoinColumn(name = "id_trybu_studiow", referencedColumnName = "id_trybu_studiow", insertable = false, updatable = false)
    private StudyMode studyMode;

    @ManyToOne
    @JoinColumn(name = "id_tytulu_zawodowego", referencedColumnName = "id_tytulu_zawodowego", insertable = false, updatable = false)
    private ProfessionalTitles professionalTitle;

    @Column(name = "liczba_semestrow", nullable = false)
    private Integer numberSemesters;

    @OneToMany(mappedBy = "coursesOfStudy")
    private Set<YearbooksStudies> yearbooksStudies;

    @OneToMany(mappedBy = "directionId")
    private Set<DirectedCoordinators> directedCoordinatorsDirections;

    @OneToMany(mappedBy = "directoryId")
    private Set<DirectedKeepers> directedKeepers;



    public Integer getIdDirection() {return idDirection;}

    public void setIdDirection(Integer idDirection) {this.idDirection = idDirection;}

    public String getNameDirection() {return nameDirection;}

    public void setNameDirection(String nameDirection) {this.nameDirection = nameDirection;}

    public String getNameSpecialty() {return nameSpecialty;}

    public void setNameSpecialty(String nameSpecialty) {this.nameSpecialty = nameSpecialty;}

    public LevelsOfStudies getLevelOfStudies() {
        return levelOfStudies;
    }

    public void setLevelOfStudies(LevelsOfStudies levelOfStudies) {
        this.levelOfStudies = levelOfStudies;
    }

    public StudyMode getStudyMode() {
        return studyMode;
    }

    public void setStudyMode(StudyMode studyMode) {
        this.studyMode = studyMode;
    }

    public ProfessionalTitles getProfessionalTitles() {
        return professionalTitle;
    }

    public void setProfessionalTitles(ProfessionalTitles professionalTitles) {
        this.professionalTitle = professionalTitles;
    }

    public Integer getNumberSemesters() {return numberSemesters;}

    public void setNumberSemesters(Integer numberSemesters) {this.numberSemesters = numberSemesters;}

    public Set<YearbooksStudies> getYearbooksStudies() {
        return yearbooksStudies;
    }

    public void setYearbooksStudies(Set<YearbooksStudies> yearbooksStudies) {
        this.yearbooksStudies = yearbooksStudies;
    }

    public ProfessionalTitles getProfessionalTitle() {
        return professionalTitle;
    }

    public void setProfessionalTitle(ProfessionalTitles professionalTitle) {
        this.professionalTitle = professionalTitle;
    }

    public Set<DirectedCoordinators> getDirectedCoordinatorsDirections() {
        return directedCoordinatorsDirections;
    }

    public void setDirectedCoordinatorsDirections(Set<DirectedCoordinators> directedCoordinatorsDirections) {
        this.directedCoordinatorsDirections = directedCoordinatorsDirections;
    }

    public Set<DirectedKeepers> getDirectedKeepers() {
        return directedKeepers;
    }

    public void setDirectedKeepers(Set<DirectedKeepers> directedKeepers) {
        this.directedKeepers = directedKeepers;
    }
}
