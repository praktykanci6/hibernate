package database.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "porozumienia", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_porozumienia"})})
public class Agreements
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agreements_sequence")
    @SequenceGenerator(name = "agreements_sequence",sequenceName = "agreements_sequence", allocationSize = 1)
    @Column(name = "id_porozumienia", nullable = false, unique = true)
    private Integer idAgreement;

    @Column(name = "nr_porozumienia", nullable = false,length = 25)
    private String numberAgreement;

    @Column(name = "data_zawarcia", nullable = false)
    private Date dateOfConclusion;

    @Column(name = "porozumienie",length = 100)
    private String agreement;

    public Integer getIdAgreement() {return idAgreement;}

    public void setIdAgreement(Integer idAgreement) {this.idAgreement = idAgreement;}

    public String getNumberAgreement() {return numberAgreement;}

    public void setNumberAgreement(String numberAgreement) {this.numberAgreement = numberAgreement;}

    public String getAgreement() {return agreement;}

    public void setAgreement(String agreement) {this.agreement = agreement;}

    public Date getDateOfConclusion() {return dateOfConclusion;}

    public void setDateOfConclusion(Date dateOfConclusion) {this.dateOfConclusion = dateOfConclusion;}
}

