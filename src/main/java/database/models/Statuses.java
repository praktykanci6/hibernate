package database.models;

import javax.persistence.*;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "statusy", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_statusu"})})
public class Statuses
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "statuses_sequence")
    @SequenceGenerator(name = "statuses_sequence",sequenceName = "statuses_sequence", allocationSize = 1)
    @Column(name = "id_statusu", nullable = false, unique = true)
    private Integer idStatuses;

    @Column(name = "status", nullable = false,length = 25)
    private String status;

    public Integer getIdStatuses() {return idStatuses;}

    public void setIdStatuses(Integer idsStatuses) {this.idStatuses = idsStatuses;}

    public String getStatus() {return status;}

    public void setStatus(String status) {this.status = status;}
}
