package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Konrad on 14-04-2016.
 */
@Entity
@Table(name = "stopnie_studiow", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_stopnia_studiow"})})
public class LevelsOfStudies {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "level_of_studies_sequence")
    @SequenceGenerator(name = "level_of_studies_sequence",sequenceName = "level_of_studies_sequence", allocationSize = 1)
    @Column(name = "id_stopnia_studiow", nullable = false, unique = true)
    private Integer levelOfStudiesId;

    @Column(name = "stopien_studiow", nullable = false, length = 30)
    private String levelOfStudies;

    @OneToMany(mappedBy = "levelOfStudies")
    private Set<CoursesOfStudy> coursesOfStudy;

    public Integer getLevelOfStudiesId() {
        return levelOfStudiesId;
    }

    public void setLevelOfStudiesId(Integer levelOfStudiesId) {
        this.levelOfStudiesId = levelOfStudiesId;
    }

    public String getLevelOfStudies() {
        return levelOfStudies;
    }

    public void setLevelOfStudies(String levelOfStudies) {
        this.levelOfStudies = levelOfStudies;
    }

    public Set<CoursesOfStudy> getCoursesOfStudy() {
        return coursesOfStudy;
    }

    public void setCoursesOfStudy(Set<CoursesOfStudy> coursesOfStudy) {
        this.coursesOfStudy = coursesOfStudy;
    }
}
