package database.models;

import javax.persistence.*;

/**
 * Created by Konrad on 20-04-2016.
 */
@Entity
@Table(name = "koordynatorzy_kierunkowi_praktykodawcow",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_praktykodawcy","id_kierunku","id_koordynatora_praktyk"})})
public class DirectedCoordinatorsOfPracticeDonors {

    @EmbeddedId
    private DirectedCoordinatorsOfPracticeDonorsId directedCoordinatorsOfPracticeDonorsId;

    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    private PracticeDonors practiceDonorsId;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false),
            @JoinColumn(name = "id_koordynatora_praktyk", referencedColumnName = "id_koordynatora_praktyk", insertable = false, updatable = false)

    })
    private DirectedCoordinators directedCoordinators;

    public DirectedCoordinatorsOfPracticeDonorsId getDirectedCoordinatorsOfPracticeDonorsId() {
        return directedCoordinatorsOfPracticeDonorsId;
    }

    public void setDirectedCoordinatorsOfPracticeDonorsId(DirectedCoordinatorsOfPracticeDonorsId directedCoordinatorsOfPracticeDonorsId) {
        this.directedCoordinatorsOfPracticeDonorsId = directedCoordinatorsOfPracticeDonorsId;
    }

    public PracticeDonors getPracticeDonorsId() {
        return practiceDonorsId;
    }

    public void setPracticeDonorsId(PracticeDonors practiceDonorsId) {
        this.practiceDonorsId = practiceDonorsId;
    }

    public DirectedCoordinators getDirectedCoordinators() {
        return directedCoordinators;
    }

    public void setDirectedCoordinators(DirectedCoordinators directedCoordinators) {
        this.directedCoordinators = directedCoordinators;
    }
}
