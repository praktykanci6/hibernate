package database.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Misiek on 13.04.2016.
 */

@Entity
@Table(name = "lata_akademickie",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_roku_akademickiego"})})
public class AcademicYears
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "academicYears_sequence")
    @SequenceGenerator(name = "academicYears_sequence",sequenceName = "academicYears_sequence", allocationSize = 1)
    @Column(name = "id_roku_akademickiego", nullable = false, unique = true)
    private Integer idAcademicYears;

    @Column(name = "rok_akademicki", nullable = false)
    private String academicYear;

    @Column(name = "data_rozpoczecia", nullable = false)
    private Date startingDate;

    @Column(name = "data_zakonczenia", nullable = false)
    private Date dateOfTermination;

    @OneToMany(mappedBy = "academicYears")
    private Set<YearbooksStudies> yearbooksStudies;

    public Integer getIdAcademicYears() {return idAcademicYears;}

    public void setIdAcademicYears(Integer idAcademicYears) {this.idAcademicYears = idAcademicYears;}

    public String getAcademicYear() {return academicYear;}

    public void setAcademicYear(String academicYear) {this.academicYear = academicYear;}

    public Date getStartingDate() {return startingDate;}

    public void setStartingDate(Date startingDate) {this.startingDate = startingDate;}

    public Date getDateOfTermination() {return dateOfTermination;}

    public void setDateOfTermination(Date dateOfTermination) {this.dateOfTermination = dateOfTermination;}

    public Set<YearbooksStudies> getYearbooksStudies() {
        return yearbooksStudies;
    }

    public void setYearbooksStudies(Set<YearbooksStudies> yearbooksStudies) {
        this.yearbooksStudies = yearbooksStudies;
    }
}
