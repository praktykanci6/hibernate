package database.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Konrad on 14-04-2016.
 */
@Embeddable
public class YearbooksStudiesId implements Serializable{
    @Column(name = "id_kierunku")
    private Integer subjectId;

    @Column(name = "id_roku_akademickiego")
    private Integer academicYearId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        YearbooksStudiesId that = (YearbooksStudiesId) o;

        if (!subjectId.equals(that.subjectId)) return false;
        return academicYearId.equals(that.academicYearId);

    }

    @Override
    public int hashCode() {
        int result = subjectId.hashCode();
        result = 31 * result + academicYearId.hashCode();
        return result;
    }
}
