package database.models;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Konrad on 02-04-2016.
 */

@Entity
@Table(name = "adresy", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_adresu"})})
public class Addresses {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "addresses_sequence")
    @SequenceGenerator(name = "addresses_sequence",sequenceName = "addresses_sequence", allocationSize = 1)
    @Column(name = "id_adresu", nullable = false, unique = true)
    private Integer addressId;

    @Column(name = "fax")
    private String fax;

    @Column(name = "kod_pocztowy", nullable = false)
    private String postCode;

    @Column(name = "kraj", nullable = false)
    private String country;

    @Column(name = "miejscowosc", nullable = false)
    private String town;

    @Column(name = "nr_budynku", nullable = false)
    private String buildingNumber;

    @Column(name = "telefon1")
    private String phoneNumber1;

    @Column(name = "telefon2")
    private String phoneNumber2;

    @Column(name = "ulica", nullable = false)
    private String street;

    @OneToMany(mappedBy = "addresses")
    private Set<Students> students;

    @OneToMany(mappedBy = "addressId")
    private Set<PracticeDonorsAddresses> practiceDonorsAddresses;

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Set<Students> getStudents() {
        return students;
    }

    public void setStudents(Set<Students> students) {
        this.students = students;
    }

    public Set<PracticeDonorsAddresses> getPracticeDonorsAddresses() {
        return practiceDonorsAddresses;
    }

    public void setPracticeDonorsAddresses(Set<PracticeDonorsAddresses> practiceDonorsAddresses) {
        this.practiceDonorsAddresses = practiceDonorsAddresses;
    }
}
