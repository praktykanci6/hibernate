package database.models;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.util.Date;

/**
 * Created by Misiek on 13.04.2016.
 */
@Entity
@Table(name = "praktyki", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_praktyki_studenckiej"})})
public class Practices
{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "practice_sequence")
    @SequenceGenerator(name = "practice_sequence",sequenceName = "practice_sequence", allocationSize = 1)

    @Column(name = "id_praktyki_studenckiej", nullable = false, unique = true)
    private Integer IdstudentInternships;

    @Column(name = "id_szablonu", nullable = false, unique = true)
    private Integer template;

    @Column(name = "id_prakdykodawcy", nullable = false, unique = true)
    private Integer idBoss;

    @Column(name = "id_statusu", nullable = false, unique = true)
    private Integer status;

    @Column(name = "data_rozpoczecia", nullable = false)
    private Date startingDate;

    @Column(name = "data_zakonczenia", nullable = false)
    private Date dateOfTermination;

    @Column(name = "id_kierunku", nullable = false,unique = true)
    private Integer idDirection;

    @Column(name = "id_typu_kierunku", nullable = false,unique = true)
    private Integer idTypeDirection;

    @Column(name = "id_roku_akademickiego", nullable = false,unique = true)
    private Integer idAcademicYear;

    @Column(name = "nr_albumu",unique = true)
    private Integer idAlbumNumber;

    @Column(name = "id_opiekuna_praktyk", nullable = false,unique = true)
    private Integer idGuardian;

    @Column(name = "id_kordynatora_praktyk", nullable = false,unique = true)
    private Integer idCoordinator;

    @Column(name = "id_adresu", nullable = false,unique = true)
    private Integer idAddress;

    @Column(name = "id_porozumienia", nullable = false,unique = true)
    private Integer idAgreement;

    @Column(name = "data_zapisu")
    private Date idDateSave;

    @Column(name = "ocena")
    private Date idRating ;

    @Column(name = "skierowanie")
    private String idReferral ;

    @Column(name = "karta_oceny")
    private String idCardRating ;


    public Integer getIdstudentInternships() {return IdstudentInternships;}

    public void setIdstudentInternships(Integer idstudentInternships) {IdstudentInternships = idstudentInternships;}

    public Integer getIdBoss() {return idBoss;}

    public void setIdBoss(Integer idBoss) {this.idBoss = idBoss;}

    public Integer getTemplate() {return template;}

    public void setTemplate(Integer template) {this.template = template;}

    public Integer getStatus() {return status;}

    public void setStatus(Integer status) {this.status = status;}

    public Date getStartingDate() {return startingDate;}

    public void setStartingDate(Date startingDate) {this.startingDate = startingDate;}

    public Date getDateOfTermination() {return dateOfTermination;}

    public void setDateOfTermination(Date dateOfTermination) {this.dateOfTermination = dateOfTermination;}

    public Integer getIdDirection() {return idDirection;}

    public void setIdDirection(Integer idDirection) {this.idDirection = idDirection;}

    public Integer getIdTypeDirection() {return idTypeDirection;}

    public void setIdTypeDirection(Integer idTypeDirection) {this.idTypeDirection = idTypeDirection;}

    public Integer getIdAcademicYear() {return idAcademicYear;}

    public void setIdAcademicYear(Integer idAcademicYear) {this.idAcademicYear = idAcademicYear;}

    public Integer getIdAlbumNumber() {return idAlbumNumber;}

    public void setIdAlbumNumber(Integer idAlbumNumber) {this.idAlbumNumber = idAlbumNumber;}

    public Integer getIdGuardian() {return idGuardian;}

    public void setIdGuardian(Integer idGuardian) {this.idGuardian = idGuardian;}

    public Integer getIdCoordinator() {return idCoordinator;}

    public void setIdCoordinator(Integer idCoordinator) {this.idCoordinator = idCoordinator;}

    public Integer getIdAddress() {return idAddress;}

    public void setIdAddress(Integer idAddress) {this.idAddress = idAddress;}

    public Integer getIdAgreement() {return idAgreement;}

    public void setIdAgreement(Integer idAgreement) {this.idAgreement = idAgreement;}

    public Date getIdDateSave() {return idDateSave;}

    public void setIdDateSave(Date idDateSave) {this.idDateSave = idDateSave;}

    public Date getIdRating() {return idRating;}

    public void setIdRating(Date idRating) {this.idRating = idRating;}

    public String getIdReferral() {return idReferral;}

    public void setIdReferral(String idReferral) {this.idReferral = idReferral;}

    public String getIdCardRating() {return idCardRating;}

    public void setIdCardRating(String idCardRating) {this.idCardRating = idCardRating;}


}
