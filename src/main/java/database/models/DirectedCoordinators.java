package database.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "koordynatorzy_kierunkowi", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_kierunku","id_koordynatora_praktyk"})})
public class DirectedCoordinators implements Serializable
{
    @EmbeddedId
    private DirectedCoordinatorsId directedCoordinatorsId;

    @ManyToOne
    @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
    private CoursesOfStudy directionId;

    @ManyToOne
    @JoinColumn(name = "id_koordynatora_praktyk", referencedColumnName = "id_koordynatora_praktyk", insertable = false, updatable = false)
    private PracticeCoordinators practiceCoordinatorId;

    @Column(name = "data_powolania", nullable = false)
    private Date appointedDate;

    public DirectedCoordinatorsId getDirectedCoordinatorsId() {
        return directedCoordinatorsId;
    }

    public void setDirectedCoordinatorsId(DirectedCoordinatorsId directedCoordinatorsId) {
        this.directedCoordinatorsId = directedCoordinatorsId;
    }

    public CoursesOfStudy getDirectionId() {
        return directionId;
    }

    public void setDirectionId(CoursesOfStudy directionId) {
        this.directionId = directionId;
    }

    public PracticeCoordinators getPracticeCoordinatorId() {
        return practiceCoordinatorId;
    }

    public void setPracticeCoordinatorId(PracticeCoordinators practiceCoordinatorId) {
        this.practiceCoordinatorId = practiceCoordinatorId;
    }

    public Date getAppointedDate() {
        return appointedDate;
    }

    public void setAppointedDate(Date appointedDate) {
        this.appointedDate = appointedDate;
    }
}