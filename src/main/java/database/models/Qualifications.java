package database.models;

import javax.persistence.*;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "kwalifikacje", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_kwalifikacji"})})
public class Qualifications
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qualifications_sequence")
    @SequenceGenerator(name = "qualifications_sequence",sequenceName = "qualifications_sequence", allocationSize = 1)
    @Column(name = "id_kwalifikacji", nullable = false, unique = true)
    private Integer idQualifications;

    @Column(name = "kwalifikacja", nullable = false)
    private Integer qualifications;

    public Integer getIdQualifications() {return idQualifications;}

    public void setIdQualifications(Integer idQualifications) {this.idQualifications = idQualifications;}

    public Integer getQualifications() {return qualifications;}

    public void setQualifications(Integer qualifications) {this.qualifications = qualifications;}
}
