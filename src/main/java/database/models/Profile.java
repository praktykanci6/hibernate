package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "profil", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_profilu"})})
public class Profile
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profile_sequence")
    @SequenceGenerator(name = "profile_sequence",sequenceName = "profile_sequence", allocationSize = 1)
    @Column(name = "id_profilu", nullable = false, unique = true)
    private Integer idProfile;

    @Column(name = "branza", nullable = false,length = 50)
    private String trade;

    @OneToMany(mappedBy = "profileId")
    private Set<PracticeDonorsProfiles> profilePracticeDonorsSet;

    public Integer getIdProfile() {return idProfile;}

    public void setIdProfile(Integer idProfile) {this.idProfile = idProfile;}

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public Set<PracticeDonorsProfiles> getProfileSet() {
        return profilePracticeDonorsSet;
    }

    public void setProfileSet(Set<PracticeDonorsProfiles> profileSet) {
        this.profilePracticeDonorsSet = profileSet;
    }
}
