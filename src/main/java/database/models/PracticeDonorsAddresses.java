package database.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Konrad on 20-04-2016.
 */
@Entity
@Table(name = "adresy_praktykodawcy")
public class PracticeDonorsAddresses implements Serializable {

    @EmbeddedId
    private PracticeDonorsProfilesId practiceDonorsProfilesId;

    @ManyToOne
    @JoinColumn(name = "id_adresu", referencedColumnName = "id_adresu", insertable = false, updatable = false)
    private Addresses addressId;

    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    private PracticeDonors practiceDonorId;

    @Column(name = "oddzial")
    private Boolean section;

    public PracticeDonorsProfilesId getPracticeDonorsProfilesId() {
        return practiceDonorsProfilesId;
    }

    public void setPracticeDonorsProfilesId(PracticeDonorsProfilesId practiceDonorsProfilesId) {
        this.practiceDonorsProfilesId = practiceDonorsProfilesId;
    }

    public Addresses getAddressId() {
        return addressId;
    }

    public void setAddressId(Addresses addressId) {
        this.addressId = addressId;
    }

    public PracticeDonors getPracticeDonorId() {
        return practiceDonorId;
    }

    public void setPracticeDonorId(PracticeDonors practiceDonorId) {
        this.practiceDonorId = practiceDonorId;
    }

    public Boolean getSection() {
        return section;
    }

    public void setSection(Boolean section) {
        this.section = section;
    }
}
