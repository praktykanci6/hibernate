package database.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Konrad on 02-04-2016.
 */

@Entity
@Table(name = "studenci",uniqueConstraints = {@UniqueConstraint(columnNames = {"id_adresu","nr_albumu","id_osoby"})})
public class Students implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "students_sequence")
    @SequenceGenerator(name = "students_sequence", sequenceName = "students_sequence", allocationSize = 1)
    @Column(name = "nr_albumu", nullable = false, unique = true)
    private Long nrAlbumu;

    @ManyToOne
    @JoinColumn(name = "id_adresu")
    private Addresses addresses;

    @OneToOne
    @JoinColumn(name = "id_osoby")
    private People people;

    @OneToMany(mappedBy = "students")
    private Set<SubjectStudents> subjectStudents;

    public Students () {}

    public Long getNrAlbumu() {
        return nrAlbumu;
    }

    public void setNrAlbumu(Long nrAlbumu) {
        this.nrAlbumu = nrAlbumu;
    }


    public Addresses getAddresses() {
        return addresses;
    }

    public void setAddresses(Addresses addresses) {
        this.addresses = addresses;
    }


    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }


    public Set<SubjectStudents> getSubjectStudents() {
        return subjectStudents;
    }

    public void setSubjectStudents(Set<SubjectStudents> subjectStudents) {
        this.subjectStudents = subjectStudents;
    }
}

