package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "praktykodawcy", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_praktykodawcy"})})
public class PracticeDonors
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "practiceDonors_sequence")
    @SequenceGenerator(name = "practiceDonors_sequence",sequenceName = "practiceDonors_sequence", allocationSize = 1)
    @Column(name = "id_praktykodawcy", nullable = false, unique = true)
    private Integer idPracticeDonor;

    @Column(name = "nazwa", nullable = false,length = 30)
    private String name;

    @Column(name = "pelna_nazwa", nullable = false,length = 100)
    private String fuulName;

    @Column(name = "zaufany")
    private Boolean confidential;

    @OneToMany(mappedBy = "practiceDonorId")
    private Set<PracticeDonorsProfiles> practiceDonorsSet;

    @OneToMany(mappedBy = "practiceDonorId")
    private Set<PracticeDonorsAddresses> practiceDonorsAddresses;

    @OneToMany(mappedBy = "practiceDonorId")
    private Set<DirectedKeepers> directedKeepers;

    public Integer getIdPracticeDonor() {
        return idPracticeDonor;
    }

    public void setIdPracticeDonor(Integer idPracticeDonor) {
        this.idPracticeDonor = idPracticeDonor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFuulName() {
        return fuulName;
    }

    public void setFuulName(String fuulName) {
        this.fuulName = fuulName;
    }

    public Boolean getConfidential() {
        return confidential;
    }

    public void setConfidential(Boolean confidential) {
        this.confidential = confidential;
    }

    public Set<PracticeDonorsProfiles> getPracticeDonorsSet() {
        return practiceDonorsSet;
    }

    public void setPracticeDonorsSet(Set<PracticeDonorsProfiles> practiceDonorsSet) {
        this.practiceDonorsSet = practiceDonorsSet;
    }

    public Set<PracticeDonorsAddresses> getPracticeDonorsAddresses() {
        return practiceDonorsAddresses;
    }

    public void setPracticeDonorsAddresses(Set<PracticeDonorsAddresses> practiceDonorsAddresses) {
        this.practiceDonorsAddresses = practiceDonorsAddresses;
    }

    public Set<DirectedKeepers> getDirectedKeepers() {
        return directedKeepers;
    }

    public void setDirectedKeepers(Set<DirectedKeepers> directedKeepers) {
        this.directedKeepers = directedKeepers;
    }
}
