package database.models;

import javax.persistence.*;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "szablony_praktyk", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_szablonu"})})
public class PracticeTemplates
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "unitCalendar_sequence")
    @SequenceGenerator(name = "unitCalendar_sequence",sequenceName = "unitCalendar_sequence", allocationSize = 1)
    @Column(name = "id_szablonu", nullable = false, unique = true)
    private Integer idTemplate;

    @Column(name = "szablon", nullable = false,length = 50)
    private String template;

    @Column(name = "opis_szablonu",length = 200)
    private String descriptionTemplate;

    public Integer getIdTemplate() {return idTemplate;}

    public void setIdTemplate(Integer idTemplate) {this.idTemplate = idTemplate;}

    public String getTemplate() {return template;}

    public void setTemplate(String template) {this.template = template;}

    public String getDescriptionTemplate() {return descriptionTemplate;}

    public void setDescriptionTemplate(String descriptionTemplate) {this.descriptionTemplate = descriptionTemplate;}
}
