package database.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Konrad on 20-04-2016.
 */
@Embeddable
public class PracticeDonorsProfilesId implements Serializable{

    @Column(name = "id_praktykodawcy")
    private Integer practiceDonorsId;

    @Column(name = "id_profilu")
    private Integer profileId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PracticeDonorsProfilesId that = (PracticeDonorsProfilesId) o;

        if (!practiceDonorsId.equals(that.practiceDonorsId)) return false;
        return profileId.equals(that.profileId);

    }

    @Override
    public int hashCode() {
        int result = practiceDonorsId.hashCode();
        result = 31 * result + profileId.hashCode();
        return result;
    }
}
