package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Konrad on 18-04-2016.
 */
@Entity
@Table(name = "koordynatorzy_praktyk", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_koordynatora_praktyk"})})
public class PracticeCoordinators {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coordinatorsPractices_sequence")
    @SequenceGenerator(name = "coordinatorsPractices_sequence",sequenceName = "coordinatorsPractices_sequence", allocationSize = 1)
    @Column(name = "id_koordynatora_praktyk", nullable = false, unique = true)
    private Integer idCoordinatorPractice;

    @OneToOne
    @JoinColumn(name = "id_osoby")
    private People person;

    @OneToMany(mappedBy = "practiceCoordinatorId")
    private Set<DirectedCoordinators> directedCoordinatorsDirections;


    public Integer getIdCoordinatorPractice() {
        return idCoordinatorPractice;
    }

    public void setIdCoordinatorPractice(Integer idCoordinatorPractice) {
        this.idCoordinatorPractice = idCoordinatorPractice;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public Set<DirectedCoordinators> getDirectedCoordinatorsDirections() {
        return directedCoordinatorsDirections;
    }

    public void setDirectedCoordinatorsDirections(Set<DirectedCoordinators> directedCoordinatorsDirections) {
        this.directedCoordinatorsDirections = directedCoordinatorsDirections;
    }
}
