package database.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Konrad on 23-04-2016.
 */
@Embeddable
public class DirectedKeepersId implements Serializable{

    @Column(name = "id_kierunku")
    private Integer directoryId;

    @Column(name = "id_opiekuna_praktyk")
    private Integer practiceKeeperId;

    @Column(name = "id_praktykodawcy")
    private Integer practiceDonorId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DirectedKeepersId that = (DirectedKeepersId) o;

        if (!directoryId.equals(that.directoryId)) return false;
        if (!practiceKeeperId.equals(that.practiceKeeperId)) return false;
        return practiceDonorId.equals(that.practiceDonorId);

    }

    @Override
    public int hashCode() {
        int result = directoryId.hashCode();
        result = 31 * result + practiceKeeperId.hashCode();
        result = 31 * result + practiceDonorId.hashCode();
        return result;
    }
}
