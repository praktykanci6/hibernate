package database.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Misiek on 16.04.2016.
 */

@Entity
@Table(name = "role", uniqueConstraints = {@UniqueConstraint(columnNames = {"id_roli"})})
public class Roles
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_sequence")
    @SequenceGenerator(name = "roles_sequence",sequenceName = "roles_sequence", allocationSize = 1)
    @Column(name = "id_roli", nullable = false, unique = true)
    private Integer idRole;

    @Column(name = "rola", nullable = false,unique = true,length = 25)
    private String role;

    @OneToMany(mappedBy = "roleId")
    private Set<Users> users;

    public Integer getIdRole() {return idRole;}

    public void setIdRole(Integer idRole) {this.idRole = idRole;}

    public String getRole() {return role;}

    public void setRole(String role) {this.role = role;}

    public Set<Users> getUsers() {
        return users;
    }

    public void setUsers(Set<Users> users) {
        this.users = users;
    }

}
